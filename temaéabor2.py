# -*- coding: utf-8 -*-

# -- Elhalálozási statisztikák --


import pandas as pd
import seaborn as sns

dead = pd.read_csv('stadat-nep0010-22.1.1.10-hu.csv', sep=";", encoding='latin1')
dead.columns

sns.relplot(x="Év", y="Halálozások száma", kind="line", hue="Nem", data=dead)

sns.relplot(x="Év", y="Szándékos önártalom", kind="line", hue="Nem", data=dead)

sns.relplot(x="Év", y="Heveny szívizom-elhalás", kind="line", hue="Nem", data=dead)

sns.relplot(x="Év", y="Egyéb ischaemiás szívbetegség", kind="line", hue="Nem", data=dead)

sns.relplot(x="Év", y="Máj-betegségek", kind="line", hue="Nem", data=dead)

sns.relplot(x="Év", y="Rosszindulatú daganatok", kind="line", hue="Nem", data=dead)

sns.relplot(x="Év", y="Motorosjármû-balesetek", kind="line", hue="Nem", data=dead)

# -- Halálozás a nemek arányában --

import pandas as pd

man_dead = pd.read_csv('Ferfi_kor.csv', sep=";", encoding='latin1')
woman_dead = pd.read_csv('No_Kor.csv', sep=";", encoding='latin1')

woman_nan = woman_dead[woman_dead.isna().any(axis=1)]
woman_nan

man_nan = man_dead[man_dead.isna().any(axis=1)]
man_nan

woman_dead_filtered = woman_dead.loc[woman_dead['Year'] > 2019]
woman_dead_filtered = woman_dead_filtered.loc[woman_dead_filtered['WeakNumber'] < 13]

man_dead_filtered = man_dead.loc[man_dead['Year'] > 2019]
man_dead_filtered = man_dead_filtered.loc[man_dead_filtered['WeakNumber'] < 13]

import seaborn as sns

sns.catplot(height=10, x="WeakNumber", y="90+", hue='Year', kind="bar", data=woman_dead_filtered)
sns.catplot(height=10, x="WeakNumber", y="90+", hue='Year', kind="bar", data=man_dead_filtered)

sns.catplot(height=10, x="WeakNumber", y="85-89", hue='Year', kind="bar", data=woman_dead_filtered)
sns.catplot(height=10, x="WeakNumber", y="85-89", hue='Year', kind="bar", data=man_dead_filtered)

sns.catplot(height=10, x="WeakNumber", y="80-84", hue='Year', kind="bar", data=woman_dead_filtered)
sns.catplot(height=10, x="WeakNumber", y="80-84", hue='Year', kind="bar", data=man_dead_filtered)

sns.catplot(height=10, x="WeakNumber", y="75-79", hue='Year', kind="bar", data=woman_dead_filtered)
sns.catplot(height=10, x="WeakNumber", y="75-79", hue='Year', kind="bar", data=man_dead_filtered)

sns.catplot(height=10, x="WeakNumber", y="70-74", hue='Year', kind="bar", data=woman_dead_filtered)
sns.catplot(height=10, x="WeakNumber", y="70-74", hue='Year', kind="bar", data=man_dead_filtered)

sns.catplot(height=10, x="WeakNumber", y="65-69", hue='Year', kind="bar", data=woman_dead_filtered)
sns.catplot(height=10, x="WeakNumber", y="65-69", hue='Year', kind="bar", data=man_dead_filtered)

sns.catplot(height=10, x="WeakNumber", y="0-34", hue='Year', kind="bar", data=woman_dead_filtered)
sns.catplot(height=10, x="WeakNumber", y="0-34", hue='Year', kind="bar", data=man_dead_filtered)

man_dead_mean = man_dead.loc[man_dead['WeakNumber'] < 13]
man_dead_mean = man_dead_mean.loc[:, man_dead_mean.columns != 'WeakNumber'].groupby(["Year"]).mean()
man_dead_mean['Year'] = man_dead_mean.index

woman_dead_mean = woman_dead.loc[woman_dead['WeakNumber'] < 13]
woman_dead_mean = woman_dead_mean.loc[:, woman_dead_mean.columns != 'WeakNumber'].groupby(["Year"]).mean()
woman_dead_mean['Year'] = woman_dead_mean.index

sns.relplot(data=woman_dead_mean.drop(["Year"],axis=1))
sns.relplot(data=man_dead_mean.drop(["Year"],axis=1))

